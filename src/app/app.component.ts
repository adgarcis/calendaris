import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  ViewChildren,
  EventEmitter,
} from '@angular/core'
//import { CalendarComponent } from 'ng-fullcalendar';
import { CalendarComponent } from 'ap-angular2-fullcalendar'

import { EventSesrvice } from './event.service'

import { Entrenador } from '../models/entrenador'
import { Equipo } from '../models/equipo'
import { Event } from '../models/event'

import * as $ from 'jquery'
import 'jqueryui'

import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  calendarOptions: Object
  displayEvent: any
  idEntrenador: string = 'all'
  events: Object[] = []

  entrenadores: Entrenador[] = []
  equipos: Equipo[] = []
  equiposEntrenador: Equipo[] = []

  //variables

  nombreEquipo: string = ''
  colorEquipo: string = ''
  eventSelect: Equipo
  //ERROR ALERT
  errorAlert: boolean = false

  parar: boolean = true

  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent
  @ViewChildren('block') blocks: any[]

  constructor(
    protected eventService: EventSesrvice,
    public element: ElementRef,
    private toastr: ToastrService,
  ) {}

  onCalendarInit(initialized: boolean) {
    console.log('Calendar initialized')
    console.log(this.ucCalendar.fullCalendar('clientEvents'))
  }

  ngOnInit() {
    if (localStorage.getItem('datos') === null) {
      var datos = {
        eventos: [
          {
            start: '2019-05-23T17:30:00',
            end: '2019-05-23T18:30:00',
            title: 'PreBenjamí',
            backgroundColor: '#ff8080',
            IdEquipo: 1223161590056,
          },
          {
            start: '2019-05-23T17:30:00',
            end: '2019-05-23T18:30:00',
            title: 'Benjamí 11',
            backgroundColor: '#ff0080',
            IdEquipo: 1841456968854,
          },
          {
            start: '2019-05-20T18:00:00',
            end: '2019-05-20T19:00:00',
            title: 'Benjamí 11',
            backgroundColor: '#ff0080',
            IdEquipo: 1841456968854,
          },
          {
            start: '2019-05-20T18:00:00',
            end: '2019-05-20T19:00:00',
            title: 'Benjamí Fem',
            backgroundColor: '#ffff80',
            IdEquipo: 3746717041698,
          },
          {
            start: '2019-05-20T18:00:00',
            end: '2019-05-20T19:00:00',
            title: 'PreBenjamí',
            backgroundColor: '#ff8080',
            IdEquipo: 1223161590056,
          },
          {
            start: '2019-05-20T19:00:00',
            end: '2019-05-20T20:00:00',
            title: 'Benjamí 10',
            backgroundColor: '#80ff80',
            IdEquipo: 9234916704656,
          },
          {
            start: '2019-05-21T19:30:00',
            end: '2019-05-21T20:30:00',
            title: 'Aleví Fem',
            backgroundColor: '#80ffff',
            IdEquipo: 2327287165057,
          },
          {
            start: '2019-05-24T17:00:00',
            end: '2019-05-24T18:00:00',
            title: 'Aleví Fem',
            backgroundColor: '#80ffff',
            IdEquipo: 2327287165057,
          },
          {
            start: '2019-05-24T17:00:00',
            end: '2019-05-24T18:00:00',
            title: 'Aleví 08',
            backgroundColor: '#ff80ff',
            IdEquipo: 2169590180435,
          },
          {
            start: '2019-05-20T19:00:00',
            end: '2019-05-20T20:00:00',
            title: 'Aleví 09',
            backgroundColor: '#0080ff',
            IdEquipo: 8812428416110,
          },
          {
            start: '2019-05-23T18:30:00',
            end: '2019-05-23T19:30:00',
            title: 'Aleví 09',
            backgroundColor: '#0080ff',
            IdEquipo: 8812428416110,
          },
          {
            start: '2019-05-22T19:30:00',
            end: '2019-05-22T20:30:00',
            title: 'Infantil 06',
            backgroundColor: '#008080',
            IdEquipo: 1688024420418,
          },
          {
            start: '2019-05-22T17:30:00',
            end: '2019-05-22T18:30:00',
            title: 'Benjamí Fem',
            backgroundColor: '#ffff80',
            IdEquipo: 3746717041698,
          },
          {
            start: '2019-05-20T19:00:00',
            end: '2019-05-20T20:00:00',
            title: 'Infantil Comarca',
            backgroundColor: '#8080ff',
            IdEquipo: 5316316384118,
          },
          {
            start: '2019-05-20T20:00:00',
            end: '2019-05-20T21:00:00',
            title: 'Cadet 05',
            backgroundColor: '#0000a0',
            IdEquipo: 5455180397235,
          },
          {
            start: '2019-05-21T20:30:00',
            end: '2019-05-21T22:00:00',
            title: 'Junior Fem',
            backgroundColor: '#808040',
            IdEquipo: 9693810021210,
          },
          {
            start: '2019-05-20T21:00:00',
            end: '2019-05-20T22:00:00',
            title: 'Senior Pref',
            backgroundColor: '#00ff00',
            IdEquipo: 7946750153178,
          },
          {
            start: '2019-05-20T20:00:00',
            end: '2019-05-20T21:00:00',
            title: 'Infantil 07',
            backgroundColor: '#ff8040',
            IdEquipo: 7854076180384,
          },
          {
            start: '2019-05-21T19:30:00',
            end: '2019-05-21T20:30:00',
            title: 'Infantil Fem',
            backgroundColor: '#ff0000',
            IdEquipo: 622659744173,
          },
          {
            start: '2019-05-21T20:30:00',
            end: '2019-05-21T22:00:00',
            title: 'Senior Aut',
            backgroundColor: '#ff8000',
            IdEquipo: 3732360484157,
          },
          {
            start: '2019-05-21T20:30:00',
            end: '2019-05-21T22:00:00',
            title: 'Senior Fem',
            backgroundColor: '#ff80c0',
            IdEquipo: 4460906888068,
          },
          {
            start: '2019-05-22T17:30:00',
            end: '2019-05-22T18:30:00',
            title: 'Aleví 08',
            backgroundColor: '#ff80ff',
            IdEquipo: 2169590180435,
          },
          {
            start: '2019-05-22T19:30:00',
            end: '2019-05-22T20:30:00',
            title: 'Alevi Comarca',
            backgroundColor: '#804040',
            IdEquipo: 3989319243876,
          },
          {
            start: '2019-05-21T19:30:00',
            end: '2019-05-21T20:30:00',
            title: 'Cadet Fem',
            backgroundColor: '#ff0080',
            IdEquipo: 1903424950247,
          },
          {
            start: '2019-05-22T18:30:00',
            end: '2019-05-22T19:30:00',
            title: 'Cadet 04',
            backgroundColor: '#008000',
            IdEquipo: 2769111386746,
          },
          {
            start: '2019-05-20T20:00:00',
            end: '2019-05-20T21:00:00',
            title: 'Junior 02',
            backgroundColor: '#ffffff',
            IdEquipo: 5371574618172,
          },
          {
            start: '2019-05-22T20:30:00',
            end: '2019-05-22T22:00:00',
            title: 'Senior Aut',
            backgroundColor: '#ff8000',
            IdEquipo: 3732360484157,
          },
          {
            start: '2019-05-22T18:30:00',
            end: '2019-05-22T19:30:00',
            title: 'Infantil Comarca',
            backgroundColor: '#8080ff',
            IdEquipo: 5316316384118,
          },
          {
            start: '2019-05-23T18:30:00',
            end: '2019-05-23T19:30:00',
            title: 'Cadet 05',
            backgroundColor: '#0000a0',
            IdEquipo: 5455180397235,
          },
          {
            start: '2019-05-23T19:30:00',
            end: '2019-05-23T20:30:00',
            title: 'Junior Fem',
            backgroundColor: '#808040',
            IdEquipo: 9693810021210,
          },
          {
            start: '2019-05-23T19:30:00',
            end: '2019-05-23T21:00:00',
            title: 'Senior Pref',
            backgroundColor: '#00ff00',
            IdEquipo: 7946750153178,
          },
          {
            start: '2019-05-23T20:30:00',
            end: '2019-05-23T22:00:00',
            title: 'Senior Fem',
            backgroundColor: '#ff80c0',
            IdEquipo: 4460906888068,
          },
          {
            start: '2019-05-24T18:00:00',
            end: '2019-05-24T19:00:00',
            title: 'Cadet 04',
            backgroundColor: '#008000',
            IdEquipo: 2769111386746,
          },
          {
            start: '2019-05-23T19:30:00',
            end: '2019-05-23T21:00:00',
            title: 'Junior 02',
            backgroundColor: '#ffffff',
            IdEquipo: 5371574618172,
          },
          {
            start: '2019-05-23T18:30:00',
            end: '2019-05-23T19:30:00',
            title: 'Infantil 07',
            backgroundColor: '#ff8040',
            IdEquipo: 7854076180384,
          },
          {
            start: '2019-05-24T18:00:00',
            end: '2019-05-24T19:30:00',
            title: 'Senior Fem',
            backgroundColor: '#ff80c0',
            IdEquipo: 4460906888068,
          },
          {
            start: '2019-05-24T19:00:00',
            end: '2019-05-24T20:00:00',
            title: 'Infantil 06',
            backgroundColor: '#008080',
            IdEquipo: 1688024420418,
          },
          {
            start: '2019-05-24T20:00:00',
            end: '2019-05-24T21:00:00',
            title: 'Cadet Fem',
            backgroundColor: '#ff0080',
            IdEquipo: 1903424950247,
          },
          {
            start: '2019-05-24T19:30:00',
            end: '2019-05-24T20:30:00',
            title: 'Infantil Fem',
            backgroundColor: '#ff0000',
            IdEquipo: 622659744173,
          },
          {
            start: '2019-05-24T18:00:00',
            end: '2019-05-24T19:00:00',
            title: 'Cadet Comarca',
            backgroundColor: '#8000ff',
            IdEquipo: 2859290262977,
          },
          {
            start: '2019-05-24T20:30:00',
            end: '2019-05-24T22:30:00',
            title: 'Senior Aut',
            backgroundColor: '#ff8000',
            IdEquipo: 3732360484157,
          },
          {
            start: '2019-05-23T17:30:00',
            end: '2019-05-23T18:30:00',
            title: 'Benjamí 10',
            backgroundColor: '#80ff80',
            IdEquipo: 9234916704656,
          },
          {
            start: '2019-05-24T17:00:00',
            end: '2019-05-24T18:00:00',
            title: 'Alevi Comarca',
            backgroundColor: '#804040',
            IdEquipo: 3989319243876,
          },
          {
            start: '2019-05-24T19:00:00',
            end: '2019-05-24T20:30:00',
            title: 'Junior 03',
            backgroundColor: '#808080',
            IdEquipo: 627026115961,
          },
          {
            start: '2019-05-22T19:30:00',
            end: '2019-05-22T20:30:00',
            title: 'Junior 03',
            backgroundColor: '#808080',
            IdEquipo: 627026115961,
          },
          {
            start: '2019-05-22T20:30:00',
            end: '2019-05-22T21:30:00',
            title: 'Cadet Comarca',
            backgroundColor: '#8000ff',
            IdEquipo: 2859290262977,
          },
        ],
        equipos: [
          {
            Entrenadores: [
              {
                nombre: 'Clara',
                IdEntrenador: 4,
              },
            ],
            color: '#ff8080',
            nombre: 'PreBenjamí',
            IdEquipo: 1223161590056,
          },
          {
            Entrenadores: [
              {
                nombre: 'Maria Vidal',
                IdEntrenador: 5,
              },
            ],
            color: '#ffff80',
            nombre: 'Benjamí Fem',
            IdEquipo: 3746717041698,
          },
          {
            Entrenadores: [
              {
                nombre: 'Noe',
                IdEntrenador: 7,
              },
              {
                nombre: 'Clara',
                IdEntrenador: 4,
              },
            ],
            color: '#80ff80',
            nombre: 'Benjamí 10',
            IdEquipo: 9234916704656,
          },
          {
            Entrenadores: [
              {
                nombre: 'Gema',
                IdEntrenador: 8,
              },
            ],
            color: '#80ffff',
            nombre: 'Aleví Fem',
            IdEquipo: 2327287165057,
          },
          {
            Entrenadores: [],
            color: '#ff80ff',
            nombre: 'Aleví 08',
            IdEquipo: 2169590180435,
          },
          {
            Entrenadores: [
              {
                nombre: 'Nacho',
                IdEntrenador: 9,
              },
              {
                nombre: 'Maria Bau',
                IdEntrenador: 10,
              },
            ],
            color: '#0080ff',
            nombre: 'Aleví 09',
            IdEquipo: 8812428416110,
          },
          {
            Entrenadores: [
              {
                nombre: 'Cristina',
                IdEntrenador: 2,
              },
            ],
            color: '#ff0000',
            nombre: 'Infantil Fem',
            IdEquipo: 622659744173,
          },
          {
            Entrenadores: [
              {
                nombre: 'Ruben',
                IdEntrenador: 11,
              },
            ],
            color: '#804040',
            nombre: 'Alevi Comarca',
            IdEquipo: 3989319243876,
          },
          {
            Entrenadores: [
              {
                nombre: 'Clara',
                IdEntrenador: 4,
              },
            ],
            color: '#ff8040',
            nombre: 'Infantil 07',
            IdEquipo: 7854076180384,
          },
          {
            Entrenadores: [
              {
                nombre: 'Santi',
                IdEntrenador: 14,
              },
            ],
            color: '#008080',
            nombre: 'Infantil 06',
            IdEquipo: 1688024420418,
          },
          {
            Entrenadores: [
              {
                nombre: 'David',
                IdEntrenador: 15,
              },
            ],
            color: '#8080ff',
            nombre: 'Infantil Comarca',
            IdEquipo: 5316316384118,
          },
          {
            Entrenadores: [],
            color: '#ff0080',
            nombre: 'Cadet Fem',
            IdEquipo: 1903424950247,
          },
          {
            Entrenadores: [],
            color: '#008000',
            nombre: 'Cadet 04',
            IdEquipo: 2769111386746,
          },
          {
            Entrenadores: [
              {
                nombre: 'Rafa',
                IdEntrenador: 16,
              },
            ],
            color: '#0000a0',
            nombre: 'Cadet 05',
            IdEquipo: 5455180397235,
          },
          {
            Entrenadores: [
              {
                nombre: 'Quique',
                IdEntrenador: 17,
              },
              {
                nombre: 'Quique',
                IdEntrenador: 17,
              },
            ],
            color: '#8000ff',
            nombre: 'Cadet Comarca',
            IdEquipo: 2859290262977,
          },
          {
            Entrenadores: [
              {
                nombre: 'Masip',
                IdEntrenador: 18,
              },
            ],
            color: '#808040',
            nombre: 'Junior Fem',
            IdEquipo: 9693810021210,
          },
          {
            Entrenadores: [
              {
                nombre: 'Paco',
                IdEntrenador: 3,
              },
            ],
            color: '#808080',
            nombre: 'Junior 03',
            IdEquipo: 627026115961,
          },
          {
            Entrenadores: [
              {
                nombre: 'Nacho',
                IdEntrenador: 9,
              },
            ],
            color: '#ffffff',
            nombre: 'Junior 02',
            IdEquipo: 5371574618172,
          },
          {
            Entrenadores: [
              {
                nombre: 'Adrian',
                IdEntrenador: 1,
              },
              {
                nombre: 'Gema',
                IdEntrenador: 8,
              },
              {
                nombre: 'Cristina',
                IdEntrenador: 2,
              },
              {
                nombre: 'Noe',
                IdEntrenador: 7,
              },
            ],
            color: '#ff80c0',
            nombre: 'Senior Fem',
            IdEquipo: 4460906888068,
          },
          {
            Entrenadores: [
              {
                nombre: 'Rafa',
                IdEntrenador: 16,
              },
              {
                nombre: 'Adrian',
                IdEntrenador: 1,
              },
            ],
            color: '#00ff00',
            nombre: 'Senior Pref',
            IdEquipo: 7946750153178,
          },
          {
            Entrenadores: [],
            color: '#ff8000',
            nombre: 'Senior Aut',
            IdEquipo: 3732360484157,
          },
          {
            Entrenadores: [
              {
                nombre: 'Irene',
                IdEntrenador: 6,
              },
              {
                nombre: 'Clara',
                IdEntrenador: 4,
              },
            ],
            color: '#ff0080',
            nombre: 'Benjamí 11',
            IdEquipo: 1841456968854,
          },
        ],
        entrenadores: [
          {
            nombre: 'Adrian',
            IdEntrenador: 1,
          },
          {
            nombre: 'Cristina',
            IdEntrenador: 2,
          },
          {
            nombre: 'Paco',
            IdEntrenador: 3,
          },
          {
            nombre: 'Clara',
            IdEntrenador: 4,
          },
          {
            nombre: 'Maria Vidal',
            IdEntrenador: 5,
          },
          {
            nombre: 'Irene',
            IdEntrenador: 6,
          },
          {
            nombre: 'Noe',
            IdEntrenador: 7,
          },
          {
            nombre: 'Gema',
            IdEntrenador: 8,
          },
          {
            nombre: 'Nacho',
            IdEntrenador: 9,
          },
          {
            nombre: 'Maria Bau',
            IdEntrenador: 10,
          },
          {
            nombre: 'Ruben',
            IdEntrenador: 11,
          },
          {
            nombre: 'Santi',
            IdEntrenador: 14,
          },
          {
            nombre: 'David',
            IdEntrenador: 15,
          },
          {
            nombre: 'Rafa',
            IdEntrenador: 16,
          },
          {
            nombre: 'Quique',
            IdEntrenador: 17,
          },
          {
            nombre: 'Masip',
            IdEntrenador: 18,
          },
        ],
      }
      localStorage.setItem('datos', JSON.stringify(datos))
    }
    this.eventService.getEvents().then((data) => {
      this.calendarOptions = {
        editable: true,
        eventLimit: false,
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listMonth',
        },
        defaultDate: '2019-05-22',
        minTime: '17:00:00',
        maxTime: '22:30:00',
        defaultView: 'agendaWeek',
        firstDay: 1,
        locale: 'es',
        droppable: true,
        drop: function (date) {
          //alert("Dropped on " + date.format());
          //console.log("drop");
        },
        eventRender: (event, element) => {
          //console.log(this.ucCalendar.fullCalendar("clientEvents"));
          if (this.idEntrenador !== 'all') {
            let idEntrenadorInt = parseInt(this.idEntrenador, 10)
            let mostrarEquipos = this.equipos.filter((equipo) => {
              if (equipo.IdEquipo === event.IdEquipo) {
                let entrenadoresEquipo = equipo.Entrenadores.filter(
                  (entrenador) => {
                    return idEntrenadorInt === entrenador.IdEntrenador
                  },
                )
                if (entrenadoresEquipo.length > 0) {
                  return true
                } else {
                  return false
                }
              } else {
                return false
              }
            })
            if (mostrarEquipos.length > 0) {
              return true
            } else {
              return false
            }
          } else {
            return true
          }
        },
        events: data.eventos,
      }
      this.equipos = data.equipos
      this.entrenadores = data.entrenadores
      this.equipos.forEach((newEquipo) => {
        setTimeout(() => {
          $('#external-events #' + newEquipo.IdEquipo + '.fc-event').each(
            function () {
              console.log('externo')
              // store data so the calendar knows to render an event upon drop

              $(this).data('event', {
                title: newEquipo.nombre, // use the element's text as the event title
                stick: true, // maintain when user navigates (see docs on the renderEvent method)
                backgroundColor: newEquipo.color,
                IdEquipo: newEquipo.IdEquipo,
                duration: '01:00',
              })

              console.log('external')
              // make the event draggable using jQuery UI
              //@ts-ignore
              $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 0, //  original position after the drag
                duration: '01:00',
              })
            },
          )
          //this.showSuccess("Creado con éxito", "Equipo");
        }, 1000)
      })
    })
  }
  clickButton(model: any) {
    this.displayEvent = model
  }
  eventClick(model: any) {
    model = {
      event: {
        id: model.event.id,
        start: model.event.start,
        end: model.event.end,
        title: model.event.title,
        allDay: model.event.allDay,
        // other params
      },
      duration: {},
    }
    this.displayEvent = model
  }
  updateEvent(model: any) {
    model = {
      event: {
        id: model.event.id,
        start: model.event.start,
        end: model.event.end,
        title: model.event.title,
        // other params
      },
      duration: {
        _data: model.duration._data,
      },
    }
    this.displayEvent = model
  }

  eventRender() {
    console.log('eventRender')
  }

  drop(equipo: Equipo) {}

  crearEntrenador(nombreEntrenador) {
    let newEntrenador: Entrenador = new Entrenador()
    newEntrenador.nombre = nombreEntrenador
    if (this.entrenadores.length === 0) {
      newEntrenador.IdEntrenador = 1
    } else {
      newEntrenador.IdEntrenador =
        this.entrenadores[this.entrenadores.length - 1].IdEntrenador + 1
    }
    this.entrenadores.push(newEntrenador)
    this.showSuccess('Creado/a con éxito', 'Entrenador/a')
  }

  showSuccess(titulo, mensaje) {
    this.toastr.success(titulo, mensaje)
  }

  showError(error) {
    this.toastr.error(error, 'Error', {
      timeOut: 3000,
    })
  }
  selectEquipoDrop(equipo) {
    this.eventSelect = equipo
  }

  crearEquipo(nombreEquipo, colorEquipo) {
    let newEquipo: Equipo = new Equipo()
    newEquipo.nombre = nombreEquipo
    /*if (this.entrenadores.length === 0) {
      newEquipo.IdEquipo = 1;
    } else {
      newEquipo.IdEquipo = this.entrenadores[this.entrenadores.length - 1].IdEntrenador + 1;
    }*/
    newEquipo.IdEquipo = Math.floor(Math.random() * 10000000000000) + 1
    newEquipo.color = colorEquipo
    this.equipos.push(newEquipo)
    setTimeout(() => {
      $('#external-events #' + newEquipo.IdEquipo + '.fc-event').each(
        function () {
          console.log('externo')
          // store data so the calendar knows to render an event upon drop

          $(this).data('event', {
            title: nombreEquipo, // use the element's text as the event title
            stick: true, // maintain when user navigates (see docs on the renderEvent method)
            backgroundColor: colorEquipo,
            IdEquipo: newEquipo.IdEquipo,
            duration: '01:00',
          })

          console.log('external')
          // make the event draggable using jQuery UI
          //@ts-ignore
          $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0, //  original position after the drag
            duration: '01:00',
          })
        },
      )
      //this.showSuccess("Creado con éxito", "Equipo");
    }, 1000)
  }

  borrarEquipo(equipo) {
    let index = this.equipos.indexOf(equipo)
    let all = this.ucCalendar.fullCalendar('clientEvents')
    all.forEach((e) => {
      if (e.IdEquipo === equipo.IdEquipo) {
        this.ucCalendar.fullCalendar('removeEvents', [e._id])
      }
    })

    this.ucCalendar.fullCalendar('refetchEvents')
    if (index !== -1) {
      this.equipos.splice(index, 1)
    } else {
      this.showError('Error al borrar')
    }
  }

  asociarEntrenadorEquipo(entrenador: Entrenador, equipo: Equipo) {
    console.log(entrenador, equipo)
    equipo.Entrenadores.push(entrenador)
  }

  desasociarEquipoEntrenador(equipo: Equipo) {
    this.equipos.forEach((e, index) => {
      if (e.IdEquipo === equipo.IdEquipo) {
        let indexEnt = e.Entrenadores.findIndex((ent) => {
          return ent.IdEntrenador === parseInt(this.idEntrenador, 10)
        })
        if (indexEnt >= 0) {
          e.Entrenadores.splice(indexEnt, 1)
          this.changeIdEntrenador(this.idEntrenador)
        }
      }
    })
  }
  changeIdEntrenador(idEntrenador) {
    this.idEntrenador = idEntrenador
    this.equiposEntrenador = this.equipos.filter((equipo) => {
      return (
        equipo.Entrenadores.filter((entrenador) => {
          return entrenador.IdEntrenador === parseInt(this.idEntrenador, 10)
        }).length > 0
      )
    })
    this.ucCalendar.fullCalendar('rerenderEvents')
    /*let allEvents = this.ucCalendar.fullCalendar("getEventSources");
    this.ucCalendar.fullCalendar("clientEvents").forEach((e)=>{
        this.ucCalendar.fullCalendar('removeEvents',[e._id]);
    })*/
  }
  delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms))
  }
  verHorariosEntrenadores() {
    this.parar = false
    this.entrenadores.forEach(async (entrenador, key) => {
      this.delay(2000 * key).then(() => {
        this.changeIdEntrenador(entrenador.IdEntrenador)
      })
    })
  }

  borrarEntrenadorEquipo(IdEntrenador, IdEquipo) {
    let equipo = this.equipos.find((e) => e.IdEquipo === IdEquipo)
    let index = equipo.Entrenadores.findIndex(
      (ent) => ent.IdEntrenador === IdEntrenador,
    )
    if(index > -1)
      equipo.Entrenadores.splice(index, 1);
  }

  guardarEventos() {
    var eventos = JSON.stringify(
      this.ucCalendar.fullCalendar('clientEvents').map(function (e) {
        return {
          start: e.start,
          end: e.end,
          title: e.title,
          backgroundColor: e.backgroundColor,
          IdEquipo: e.IdEquipo,
        }
      }),
    )
    var datos = {
      eventos: JSON.parse(eventos),
      equipos: this.equipos,
      entrenadores: this.entrenadores,
    }
    localStorage.setItem('datos', JSON.stringify(datos))

    if (localStorage.getItem('datos') === JSON.stringify(datos)) {
      this.showSuccess('Guardado con éxito', 'Guardar')
    } else {
      this.showError('Error al guardar')
    }
  }
  getEventsByEquipo(IdEquipo): number{
    if(!this.ucCalendar){
      return 0
    }
    let all = this.ucCalendar.fullCalendar('clientEvents');
    console.log(all)
    return all.filter(e => e.IdEquipo === IdEquipo).length;
  }

  eliminarEntrenador(idEntrenador: string) {
    let idEntrenadorInt = parseInt(idEntrenador, 10);
    let equipos = this.equipos.filter(e => e.Entrenadores.filter(entr => entr.IdEntrenador === idEntrenadorInt));
    equipos.forEach(eq => {
      this.desasociarEquipoEntrenador(eq);
    })
    this.entrenadores = this.entrenadores.filter(e => e.IdEntrenador !== idEntrenadorInt);
    this.idEntrenador = 'all';
    this.changeIdEntrenador('all');
    this.guardarEventos();
  }
  exportar(){
    var weekday = new Array(7);
    weekday[0] = "Diumenge";
    weekday[1] = "Dilluns";
    weekday[2] = "Dimarts";
    weekday[3] = "Dimecres";
    weekday[4] = "Dijous";
    weekday[5] = "Divendres";
    weekday[6] = "Dissabte";
    var txt = '';
    JSON.parse(localStorage.getItem('datos')).eventos.forEach(e =>{
        txt += e.title + ' \t ' + weekday[new Date(e.start).getDay()] +  ' \t ' + e.start.split('T')[1] + ' \t ' + e.end.split('T')[1] + ' \n '
    });
    console.log(txt);
  }
}

/*
{"eventos":[{"start":"2019-05-23T17:30:00","end":"2019-05-23T18:30:00","title":"PreBenjamí","backgroundColor":"#ff8080","IdEquipo":1223161590056},{"start":"2019-05-23T17:30:00","end":"2019-05-23T18:30:00","title":"Benjamí 11","backgroundColor":"#ff0080","IdEquipo":1841456968854},{"start":"2019-05-20T18:00:00","end":"2019-05-20T19:00:00","title":"Benjamí 11","backgroundColor":"#ff0080","IdEquipo":1841456968854},{"start":"2019-05-20T18:00:00","end":"2019-05-20T19:00:00","title":"Benjamí Fem","backgroundColor":"#ffff80","IdEquipo":3746717041698},{"start":"2019-05-20T18:00:00","end":"2019-05-20T19:00:00","title":"PreBenjamí","backgroundColor":"#ff8080","IdEquipo":1223161590056},{"start":"2019-05-20T19:00:00","end":"2019-05-20T20:00:00","title":"Benjamí 10","backgroundColor":"#80ff80","IdEquipo":9234916704656},{"start":"2019-05-21T19:30:00","end":"2019-05-21T20:30:00","title":"Aleví Fem","backgroundColor":"#80ffff","IdEquipo":2327287165057},{"start":"2019-05-24T17:00:00","end":"2019-05-24T18:00:00","title":"Aleví Fem","backgroundColor":"#80ffff","IdEquipo":2327287165057},{"start":"2019-05-24T17:00:00","end":"2019-05-24T18:00:00","title":"Aleví 08","backgroundColor":"#ff80ff","IdEquipo":2169590180435},{"start":"2019-05-20T19:00:00","end":"2019-05-20T20:00:00","title":"Aleví 09","backgroundColor":"#0080ff","IdEquipo":8812428416110},{"start":"2019-05-23T18:30:00","end":"2019-05-23T19:30:00","title":"Aleví 09","backgroundColor":"#0080ff","IdEquipo":8812428416110},{"start":"2019-05-22T19:30:00","end":"2019-05-22T20:30:00","title":"Infantil 06","backgroundColor":"#008080","IdEquipo":1688024420418},{"start":"2019-05-22T17:30:00","end":"2019-05-22T18:30:00","title":"Benjamí Fem","backgroundColor":"#ffff80","IdEquipo":3746717041698},{"start":"2019-05-20T19:00:00","end":"2019-05-20T20:00:00","title":"Infantil Comarca","backgroundColor":"#8080ff","IdEquipo":5316316384118},{"start":"2019-05-20T20:00:00","end":"2019-05-20T21:00:00","title":"Cadet 05","backgroundColor":"#0000a0","IdEquipo":5455180397235},{"start":"2019-05-21T20:30:00","end":"2019-05-21T22:00:00","title":"Junior Fem","backgroundColor":"#808040","IdEquipo":9693810021210},{"start":"2019-05-20T21:00:00","end":"2019-05-20T22:00:00","title":"Senior Pref","backgroundColor":"#00ff00","IdEquipo":7946750153178},{"start":"2019-05-20T20:00:00","end":"2019-05-20T21:00:00","title":"Infantil 07","backgroundColor":"#ff8040","IdEquipo":7854076180384},{"start":"2019-05-21T19:30:00","end":"2019-05-21T20:30:00","title":"Infantil Fem","backgroundColor":"#ff0000","IdEquipo":622659744173},{"start":"2019-05-21T20:30:00","end":"2019-05-21T22:00:00","title":"Senior Aut","backgroundColor":"#ff8000","IdEquipo":3732360484157},{"start":"2019-05-21T20:30:00","end":"2019-05-21T22:00:00","title":"Senior Fem","backgroundColor":"#ff80c0","IdEquipo":4460906888068},{"start":"2019-05-22T17:30:00","end":"2019-05-22T18:30:00","title":"Aleví 08","backgroundColor":"#ff80ff","IdEquipo":2169590180435},{"start":"2019-05-22T19:30:00","end":"2019-05-22T20:30:00","title":"Alevi Comarca","backgroundColor":"#804040","IdEquipo":3989319243876},{"start":"2019-05-21T19:30:00","end":"2019-05-21T20:30:00","title":"Cadet Fem","backgroundColor":"#ff0080","IdEquipo":1903424950247},{"start":"2019-05-22T18:30:00","end":"2019-05-22T19:30:00","title":"Cadet 04","backgroundColor":"#008000","IdEquipo":2769111386746},{"start":"2019-05-20T20:00:00","end":"2019-05-20T21:00:00","title":"Junior 02","backgroundColor":"#ffffff","IdEquipo":5371574618172},{"start":"2019-05-22T20:30:00","end":"2019-05-22T22:00:00","title":"Senior Aut","backgroundColor":"#ff8000","IdEquipo":3732360484157},{"start":"2019-05-22T18:30:00","end":"2019-05-22T19:30:00","title":"Infantil Comarca","backgroundColor":"#8080ff","IdEquipo":5316316384118},{"start":"2019-05-23T18:30:00","end":"2019-05-23T19:30:00","title":"Cadet 05","backgroundColor":"#0000a0","IdEquipo":5455180397235},{"start":"2019-05-23T19:30:00","end":"2019-05-23T20:30:00","title":"Junior Fem","backgroundColor":"#808040","IdEquipo":9693810021210},{"start":"2019-05-23T19:30:00","end":"2019-05-23T21:00:00","title":"Senior Pref","backgroundColor":"#00ff00","IdEquipo":7946750153178},{"start":"2019-05-23T20:30:00","end":"2019-05-23T22:00:00","title":"Senior Fem","backgroundColor":"#ff80c0","IdEquipo":4460906888068},{"start":"2019-05-24T18:00:00","end":"2019-05-24T19:00:00","title":"Cadet 04","backgroundColor":"#008000","IdEquipo":2769111386746},{"start":"2019-05-23T19:30:00","end":"2019-05-23T21:00:00","title":"Junior 02","backgroundColor":"#ffffff","IdEquipo":5371574618172},{"start":"2019-05-23T18:30:00","end":"2019-05-23T19:30:00","title":"Infantil 07","backgroundColor":"#ff8040","IdEquipo":7854076180384},{"start":"2019-05-24T18:00:00","end":"2019-05-24T19:30:00","title":"Senior Fem","backgroundColor":"#ff80c0","IdEquipo":4460906888068},{"start":"2019-05-24T19:00:00","end":"2019-05-24T20:00:00","title":"Infantil 06","backgroundColor":"#008080","IdEquipo":1688024420418},{"start":"2019-05-24T20:00:00","end":"2019-05-24T21:00:00","title":"Cadet Fem","backgroundColor":"#ff0080","IdEquipo":1903424950247},{"start":"2019-05-24T19:30:00","end":"2019-05-24T20:30:00","title":"Infantil Fem","backgroundColor":"#ff0000","IdEquipo":622659744173},{"start":"2019-05-24T18:00:00","end":"2019-05-24T19:00:00","title":"Cadet Comarca","backgroundColor":"#8000ff","IdEquipo":2859290262977},{"start":"2019-05-24T20:30:00","end":"2019-05-24T22:30:00","title":"Senior Aut","backgroundColor":"#ff8000","IdEquipo":3732360484157},{"start":"2019-05-23T17:30:00","end":"2019-05-23T18:30:00","title":"Benjamí 10","backgroundColor":"#80ff80","IdEquipo":9234916704656},{"start":"2019-05-24T17:00:00","end":"2019-05-24T18:00:00","title":"Alevi Comarca","backgroundColor":"#804040","IdEquipo":3989319243876},{"start":"2019-05-24T19:00:00","end":"2019-05-24T20:30:00","title":"Junior 03","backgroundColor":"#808080","IdEquipo":627026115961},{"start":"2019-05-22T19:30:00","end":"2019-05-22T20:30:00","title":"Junior 03","backgroundColor":"#808080","IdEquipo":627026115961},{"start":"2019-05-22T20:30:00","end":"2019-05-22T21:30:00","title":"Cadet Comarca","backgroundColor":"#8000ff","IdEquipo":2859290262977}],"equipos":[{"Entrenadores":[{"nombre":"Clara","IdEntrenador":4}],"color":"#ff8080","nombre":"PreBenjamí","IdEquipo":1223161590056},{"Entrenadores":[{"nombre":"Maria Vidal","IdEntrenador":5}],"color":"#ffff80","nombre":"Benjamí Fem","IdEquipo":3746717041698},{"Entrenadores":[{"nombre":"Noe","IdEntrenador":7},{"nombre":"Clara","IdEntrenador":4}],"color":"#80ff80","nombre":"Benjamí 10","IdEquipo":9234916704656},{"Entrenadores":[{"nombre":"Gema","IdEntrenador":8}],"color":"#80ffff","nombre":"Aleví Fem","IdEquipo":2327287165057},{"Entrenadores":[],"color":"#ff80ff","nombre":"Aleví 08","IdEquipo":2169590180435},{"Entrenadores":[{"nombre":"Nacho","IdEntrenador":9},{"nombre":"Maria Bau","IdEntrenador":10}],"color":"#0080ff","nombre":"Aleví 09","IdEquipo":8812428416110},{"Entrenadores":[{"nombre":"Cristina","IdEntrenador":2}],"color":"#ff0000","nombre":"Infantil Fem","IdEquipo":622659744173},{"Entrenadores":[{"nombre":"Ruben","IdEntrenador":11}],"color":"#804040","nombre":"Alevi Comarca","IdEquipo":3989319243876},{"Entrenadores":[{"nombre":"Clara","IdEntrenador":4}],"color":"#ff8040","nombre":"Infantil 07","IdEquipo":7854076180384},{"Entrenadores":[{"nombre":"Santi","IdEntrenador":14}],"color":"#008080","nombre":"Infantil 06","IdEquipo":1688024420418},{"Entrenadores":[{"nombre":"David","IdEntrenador":15}],"color":"#8080ff","nombre":"Infantil Comarca","IdEquipo":5316316384118},{"Entrenadores":[],"color":"#ff0080","nombre":"Cadet Fem","IdEquipo":1903424950247},{"Entrenadores":[],"color":"#008000","nombre":"Cadet 04","IdEquipo":2769111386746},{"Entrenadores":[{"nombre":"Rafa","IdEntrenador":16}],"color":"#0000a0","nombre":"Cadet 05","IdEquipo":5455180397235},{"Entrenadores":[{"nombre":"Quique","IdEntrenador":17},{"nombre":"Quique","IdEntrenador":17}],"color":"#8000ff","nombre":"Cadet Comarca","IdEquipo":2859290262977},{"Entrenadores":[{"nombre":"Masip","IdEntrenador":18}],"color":"#808040","nombre":"Junior Fem","IdEquipo":9693810021210},{"Entrenadores":[{"nombre":"Paco","IdEntrenador":3}],"color":"#808080","nombre":"Junior 03","IdEquipo":627026115961},{"Entrenadores":[{"nombre":"Nacho","IdEntrenador":9}],"color":"#ffffff","nombre":"Junior 02","IdEquipo":5371574618172},{"Entrenadores":[{"nombre":"Adrian","IdEntrenador":1},{"nombre":"Gema","IdEntrenador":8},{"nombre":"Cristina","IdEntrenador":2},{"nombre":"Noe","IdEntrenador":7}],"color":"#ff80c0","nombre":"Senior Fem","IdEquipo":4460906888068},{"Entrenadores":[{"nombre":"Rafa","IdEntrenador":16},{"nombre":"Adrian","IdEntrenador":1}],"color":"#00ff00","nombre":"Senior Pref","IdEquipo":7946750153178},{"Entrenadores":[],"color":"#ff8000","nombre":"Senior Aut","IdEquipo":3732360484157},{"Entrenadores":[{"nombre":"Irene","IdEntrenador":6},{"nombre":"Clara","IdEntrenador":4}],"color":"#ff0080","nombre":"Benjamí 11","IdEquipo":1841456968854}],"entrenadores":[{"nombre":"Adrian","IdEntrenador":1},{"nombre":"Cristina","IdEntrenador":2},{"nombre":"Paco","IdEntrenador":3},{"nombre":"Clara","IdEntrenador":4},{"nombre":"Maria Vidal","IdEntrenador":5},{"nombre":"Irene","IdEntrenador":6},{"nombre":"Noe","IdEntrenador":7},{"nombre":"Gema","IdEntrenador":8},{"nombre":"Nacho","IdEntrenador":9},{"nombre":"Maria Bau","IdEntrenador":10},{"nombre":"Ruben","IdEntrenador":11},{"nombre":"Cristina","IdEntrenador":12},{"nombre":"Clara","IdEntrenador":13},{"nombre":"Santi","IdEntrenador":14},{"nombre":"David","IdEntrenador":15},{"nombre":"Rafa","IdEntrenador":16},{"nombre":"Quique","IdEntrenador":17},{"nombre":"Masip","IdEntrenador":18}]}
*/
