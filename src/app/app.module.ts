import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

//import { FullCalendarModule } from 'ng-fullcalendar';
import { CalendarModule } from "ap-angular2-fullcalendar";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';



import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { EventSesrvice } from './event.service';
@NgModule({
  imports: [BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot(),
    CalendarModule],
  declarations: [AppComponent, HelloComponent],
  bootstrap: [AppComponent],
  providers: [EventSesrvice]
})
export class AppModule { }
