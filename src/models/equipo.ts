import {Entrenador} from './entrenador'

interface EquipoInterface {
  nombre: string,
  IdEquipo: number,
  Entrenadores: Entrenador[],
  color: string
}

export class Equipo implements EquipoInterface{
  nombre: string;
  IdEquipo: number;
  Entrenadores: Entrenador[] = [];
  color: string = "#FFFFFF";
  constructor(){

  }
}
