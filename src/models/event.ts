interface EventInterface {
  event: EventModel,
  duration: any
}

interface EventModelInterface {
  id: number,
  start: Date,
  end: Date,
  backgroundColor: string,
  borderColor: string,
  IdEquip: number,
  title: string
}

export class EventModel implements EventModelInterface{
    id: number;
    start: Date;
    end: Date;
    backgroundColor: string;
    borderColor: string;
    IdEquip: number;
    title: string;
    constructor(){

    }
}

export class Event implements EventInterface{
    event: EventModel = new EventModel();
    duration: Date;
    constructor(){

    }
}
